﻿using UnityEngine;

using System.Collections.Generic;

public class LevelBuilder : MonoBehaviour
{
	public GameObject wallPrefab;
	public GameObject floorPrefab;

	public int minRoomWidth		= 5;
	public int maxRoomWidth		= 10;
	public int minRoomHeight	= 5;
	public int maxRoomHeight	= 10;
	public int minRooms			= 3;
	public int maxRooms			= 10;
	public int space			= 5;

	private List<Room> rooms = new List<Room>();

	private Dictionary<int, List<Directions>> roomDirectionsDict = new Dictionary<int, List<Directions>>();

	private void Start()
	{
		BuildRooms();
	}

	private void BuildRooms()
	{
		int roomsAmount = Random.Range(minRooms, maxRooms + 1);
		
		for (int i = 0; i < roomsAmount; i++)
		{
			Directions originDir = Directions.NONE;
			
			Vector2Int newRoomPos = Vector2Int.zero;
			Vector2Int newRoomSize = new Vector2Int(Random.Range(minRoomWidth, maxRoomWidth + 1), Random.Range(minRoomHeight, maxRoomHeight + 1));

			if (i != 0)
			{
				bool rectOverlap = true;

				while (rectOverlap)
				{
					int roomSibling = -1;
					while (roomSibling == -1)
					{
						roomSibling = Random.Range(0, i);
						if (roomDirectionsDict[roomSibling].Count == 0)
							roomSibling = -1;
					}

					originDir = GetOriginDir(roomSibling);
					RectInt siblingRect	= rooms[roomSibling].GetRect();
			
					switch (originDir)
					{
						case Directions.LEFT:
							newRoomPos.x = siblingRect.x - space - newRoomSize.x;
							newRoomPos.y = (int)siblingRect.center.y - (newRoomSize.y / 2); 
						break;
						case Directions.RIGHT:
							newRoomPos.x = siblingRect.xMax + space;
							newRoomPos.y = (int)siblingRect.center.y - (newRoomSize.y / 2);
						break;
						case Directions.TOP:
							newRoomPos.x = (int)siblingRect.center.x - (newRoomSize.x / 2); 
							newRoomPos.y = (siblingRect.y + siblingRect.height) + space;
						break;
						case Directions.BOTTOM:
							newRoomPos.x = (int)siblingRect.center.x - (newRoomSize.x / 2); 
							newRoomPos.y = siblingRect.y - space - newRoomSize.y;;
						break;
					}

					rectOverlap = OverlapsWithAnyRoom(new RectInt(newRoomPos, newRoomSize));
				}
			}
			
			CreateRoom(i, newRoomPos, newRoomSize, originDir);
		}
	}

	private bool OverlapsWithAnyRoom(RectInt rect)
	{
		for (int i = 0; i < rooms.Count; i++)
		{
			if (Utils.RectIntOverlaps(rooms[i].GetRect(), rect))
				return true;
		}

		return false;
	}

	private Directions GetOriginDir(int roomSibling)
	{
		int randIndex = Random.Range(0, roomDirectionsDict[roomSibling].Count);
		Directions dir = roomDirectionsDict[roomSibling][randIndex];
		roomDirectionsDict[roomSibling].RemoveAt(randIndex);
		return dir;
	}

	private void CreateRoom(int index, Vector2Int pos, Vector2Int size, Directions originDirection)
	{
		GameObject roomGO = new GameObject("room_" + index);
		Room room = roomGO.AddComponent<Room>();

		//The Room is created from bottom to top and from left to right
		RectInt rect = new RectInt(pos, size);
		room.Init(index, rect);

		// Debug.Log(string.Format("Room {0} | Pos = {1} | Size = {2} | xyMin = {3},{4} | xyMax = {5},{6} | MinCorner = {7} | MaxCorner = {8}",
		// index, rect.position, rect.size, rect.xMin, rect.yMin, rect.xMax, rect.yMax, rect.min, rect.max));

		for (int y = rect.y; y <= rect.yMax; y++)
		{
			for (int x = rect.x; x <= rect.xMax; x++)
			{
				if (x == rect.x || x == rect.xMax || y == rect.y || y == rect.yMax)
					CreateItem(x, y, wallPrefab, roomGO.transform);
				else
					CreateItem(x, y, floorPrefab, roomGO.transform);
			}
		}

		rooms.Add(room);

		//Add available directions in case that conects with other rooms
		List<Directions> availableDirections = new List<Directions>();
		Directions inverseOriginDir = Utils.GetInverseDirection(originDirection);
		for (int i = 0; i < (int)Directions.NONE; i++)
		{
			Directions dir = (Directions)i;
			if (dir != inverseOriginDir)
				availableDirections.Add(dir);
		}

		roomDirectionsDict.Add(index, availableDirections);
	}

	private void CreateItem(int x, int y, GameObject prefab, Transform parent)
	{
		Instantiate(prefab, new Vector3(x, y, 0), Quaternion.identity, parent);
	}

	private void ClearLevelAndRebuild()
	{
		for (int i = 0; i < rooms.Count; i++)
		{
			Destroy(rooms[i].gameObject);
		}

		rooms.Clear();
		roomDirectionsDict.Clear();

		BuildRooms();
	}

	private float currentTime = 0f;
	public float duration = 1.5f;
	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.R))
			ClearLevelAndRebuild();
	
		currentTime += Time.unscaledDeltaTime;
		if (currentTime >= duration)
		{
			currentTime = 0f;
			ClearLevelAndRebuild();
		}
	}
}