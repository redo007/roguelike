using UnityEngine;

public class Utils
{
    public static Directions GetInverseDirection(Directions dir)
    {
        switch (dir)
        {
            case Directions.LEFT:   return Directions.RIGHT;
            case Directions.RIGHT:  return Directions.LEFT;
            case Directions.TOP:    return Directions.BOTTOM;
            case Directions.BOTTOM: return Directions.TOP;
        }

        return Directions.NONE;
    }

    public static bool RectIntOverlaps(RectInt rectA, RectInt rectB)
    {
        Rect a = new Rect(rectA.position, rectA.size);
        Rect b = new Rect(rectB.position, rectB.size);

        return a.Overlaps(b);
    }
}