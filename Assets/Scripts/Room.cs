﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
	private int index	= -1;
	
	private RectInt rect;

	public void Init(int index, RectInt rect)
	{
		this.index	= index;
		this.rect	= rect;
	}

	public int GetIndex()
	{
		return index;
	}

	public RectInt GetRect()
	{
		return rect;
	}
}